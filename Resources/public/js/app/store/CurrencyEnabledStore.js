Ext.define('WebitCommonCurrency.store.CurrencyEnabledStore',{
	extend: 'Ext.data.Store',
	model: 'WebitCommonCurrency.model.CurrencyEnabled',
	remoteSort: false,
	remoteFilter: false,
	remoteGroup: false,
	sorters: [{
		property: 'base',
		direction: 'DESC'
	},{
		property: 'code',
		direction: 'ASC'
	}]
});
