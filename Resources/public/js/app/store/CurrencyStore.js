Ext.define('WebitCommonCurrency.store.CurrencyStore',{
	extend: 'Ext.data.Store',
	model: 'WebitCommonCurrency.model.Currency',
	remoteSort: false,
	remoteFilter: false,
	remoteGroup: false,
	sorters: [{
		property: 'code',
		direction: 'ASC'
	}]
});
