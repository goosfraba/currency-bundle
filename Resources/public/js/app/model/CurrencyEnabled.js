Ext.define('WebitCommonCurrency.model.CurrencyEnabled',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id'
	},{
		name: 'label',
		type: 'string',
		persist: false
	},{
		name: 'symbol',
		type: 'string',
		persist: false
	},{
		name: 'code',
		type: 'string'
	},{
		name: 'base',
		type: 'bool',
		defaultValue: false
	},{
		name: 'currency',
		model: 'WebitCommonCurrency.model.Currency',
		persist: false
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector('webit_common_currency.extjs_currency_enabled_store'),
		reader: {
      type: 'json',
      root: 'data',
      successProperty : 'success',
      totalProperty : 'total',
      idProperty : 'id'
    },
    writer : {
    	type : 'json',
    	writeAllFields : true,
    	allowSingle : false
    }
	}
});
