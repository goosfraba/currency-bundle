Ext.define('WebitCommonCurrency.controller.Front',{
	extend: 'Ext.app.Controller',
	views: [
		'WebitCommonCurrency.view.CurrencyMainPanel',
		'WebitCommonCurrency.view.CurrencyGrid',
		'WebitCommonCurrency.view.CurrencyCombo',
		'WebitCommonCurrency.view.CurrencyEnabledGrid',
		'WebitCommonCurrency.view.CurrencyEnabledCombo'
	],
	models: [
		'WebitCommonCurrency.model.Currency',
		'WebitCommonCurrency.model.CurrencyEnabled'
	],
	stores: [
		'WebitCommonCurrency.store.CurrencyStore',
		'WebitCommonCurrency.store.CurrencyEnabledStore'
	],
	init: function() {
		this.control({
			'webit_common_currency_currency_grid': {
				afterrender: function(grid) {
					grid.getStore().load();
				},
				beforeedit: this.beforeCurrencyEdit
			},
			'webit_common_currency_currency_enabled_grid': {
				afterrender: function(grid) {
					grid.getStore().load();
					grid.getStore().on('write',this.onCurrencyEnabledWrite);
				},
				beforeedit: this.beforeCurrencyEnabledEdit
			}
		});
	},
	beforeCurrencyEdit: function(editor, e, opts) {
		if(e.record.phantom == false) {
			e.grid.columns[0].getEditor().setReadOnly(true);
		} else {
			e.grid.columns[0].getEditor().setReadOnly(false);
		}
	},
	beforeCurrencyEnabledEdit: function(editor, e, opts) {
		var codeEditor = e.grid.columns[0].getEditor();
		if(e.record.phantom == false) {
			codeEditor.setReadOnly(true);
			if(codeEditor.getStore().findRecord('code',e.record.get('code')) == null) {
				codeEditor.getStore().insert(0,e.record.get('currency'));
			}
		} else {
			e.grid.columns[0].getEditor().setReadOnly(false);
		}
	},
	onCurrencyEnabledWrite: function(store, operation, eOpts) {
		if(operation.action == 'create' || operation.action == 'update') {
			var r = operation.records[0];
			if(r.get('base') == true) {
				store.suspendAutoSync()
				store.each(function(cur) {
					if(cur.get('code') != r.get('code')) {
						cur.set('base',false);
					}
				});
				store.commitChanges();
				store.resumeAutoSync();
			}
		}
	}
});
