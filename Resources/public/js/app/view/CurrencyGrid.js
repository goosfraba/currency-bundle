Ext.define('WebitCommonCurrency.view.CurrencyGrid',{
	extend: 'Webit.view.grid.EditableGrid',
	alias: 'widget.webit_common_currency_currency_grid',
	initComponent: function() {
		Ext.apply(this,{
			editmode: 'row',
			columns: [{
				header: 'Kod',
				dataIndex: 'code',
				width: 80,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}
			},{
				header: 'Symbol',
				dataIndex: 'symbol',
				width: 80,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}
			},{
				header: 'Nazwa',
				dataIndex: 'label',
				editor: {
					xtype: 'textfield',
					allowBlank: false
				},
				flex: 1
			}],
			store: {
				model: 'WebitCommonCurrency.model.Currency',
				autoSync: true,
				remoteSort: false,
				remoteFilter: false,
				remoteGroup: false,
				sorters: [{
					property: 'code',
					direction: 'ASC'
				}]
			}
		});
		
		this.callParent();
	}
});