Ext.define('WebitCommonCurrency.view.CurrencyCombo',{
	extend: 'Ext.form.ComboBox',
	alias: 'widget.webit_common_currency_currency_combo',
	fieldLabel: 'Waluta',
	valueField: 'code',
	displayField: 'code',
	forceSelection: true,
	editable: false,
	dataMode: 'ajax',
	initComponent: function() {
		if(this.dataMode == 'ajax') {
			Ext.apply(this,{
				store: 'WebitCommonCurrency.store.CurrencyStore'
			})
		} else {
			Ext.apply(this,{
				queryMode: 'local',
				store: {
					model: 'WebitCommonCurrency.model.Currency',
					data: Webit.data.StaticData.getData('currency')
				}
			});
		}
		
		this.callParent();
	}
});
