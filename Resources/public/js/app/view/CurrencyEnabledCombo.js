Ext.define('WebitCommonCurrency.view.CurrencyEnabledCombo',{
	extend: 'Ext.form.ComboBox',
	alias: 'widget.webit_common_currency_currency_enabled_combo',
	fieldLabel: 'Waluta',
	store: 'WebitCommonCurrency.store.CurrencyEnabledStore',
	valueField: 'code',
	displayField: 'code',
	forceSelection: true,
	editable: false,
	dataMode: 'ajax',
	initComponent: function() {
		if(this.dataMode == 'ajax') {
			Ext.apply(this,{
				store: 'WebitCommonCurrency.store.CurrencyEnabledStore'
			})
		} else {
			Ext.apply(this,{
				queryMode: 'local',
				store: {
					model: 'WebitCommonCurrency.model.CurrencyEnabled',
					data: Webit.data.StaticData.getData('currency_enabled')
				}
			});
		}
		
		this.callParent();
	}
});