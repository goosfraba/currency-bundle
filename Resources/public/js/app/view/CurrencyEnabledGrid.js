Ext.define('WebitCommonCurrency.view.CurrencyEnabledGrid',{
	extend: 'Webit.view.grid.EditableGrid',
	alias: 'widget.webit_common_currency_currency_enabled_grid',
	initComponent: function() {
		Ext.apply(this,{
			editmode: 'row',
			columns: [{
				header: 'Waluta',
				dataIndex: 'code',
				width: 120,
				editor: {
					xtype: 'webit_common_currency_currency_combo',
					allowBlank: false,
					fieldLabel: false,
					listeners: {
		        beforequery: function(qe) {
		        	delete qe.combo.lastQuery;
		        }
    			}
				}
			},{
				header: 'Bazowa',
				dataIndex: 'base',
				renderer: function(v, meta, r) {					
					return v ? 'Tak' : 'Nie';
				},
				flex: 1,
				editor: {
					xtype: 'checkbox',
					style: 'text-align:left'
				}
			}],
			store: {
				model: 'WebitCommonCurrency.model.CurrencyEnabled',
				autoSync: true,
				remoteSort: false,
				remoteFilter: false,
				remoteGroup: false,
				sorters: [{
					property: 'base',
					direction: 'DESC'
				},{
					property: 'code',
					direction: 'ASC'
				}]
			}
		});
		
		this.callParent();
	}
});