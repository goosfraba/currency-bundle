Ext.define('WebitCommonCurrency.view.CurrencyMainPanel',{
	extend: 'Ext.tab.Panel',
	alias: 'widget.webit_common_currency_currency_main_panel',
	initComponent: function() {
		Ext.apply(this,{
			items: [{
				xtype: 'webit_common_currency_currency_enabled_grid',
				title: 'Dostępne waluty'
			},{
				xtype: 'webit_common_currency_currency_grid',
				title: 'Waluty'
			}]
		});
		
		this.callParent();
	}
})