<?php

namespace Webit\Common\CurrencyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('webit_common_currency');
				$rootNode
					->children()
						->scalarNode('use_extjs')->defaultTrue()->end()
						->arrayNode('currency')->addDefaultsIfNotSet()
							->children()
								->scalarNode('dictionary_name')->defaultValue('currency')->end()
								->scalarNode('storage_type')->defaultValue('orm')->end()
								->arrayNode('orm')->addDefaultsIfNotSet()
									->children()
										->scalarNode('item_class')->defaultValue('Webit\Common\CurrencyBundle\Entity\Currency')->end()
									->end()
								->end()
								->arrayNode('phpcr')->addDefaultsIfNotSet()
									->children()
										->scalarNode('item_class')->defaultValue('Webit\Common\CurrencyBundle\Document\Currency')->end()
										->scalarNode('root')->defaultValue('/currency/list')->end()
									->end()
								->end()
							->end()
						->end()
						->arrayNode('currency_enabled')->addDefaultsIfNotSet()
							->children()
								->scalarNode('dictionary_name')->defaultValue('currency_enabled')->end()
								->scalarNode('dictionary_class')->defaultValue('Webit\Common\CurrencyBundle\Model\CurrencyEnabledDictionary')->end()
								->scalarNode('storage_type')->defaultValue('orm')->end()
								->arrayNode('orm')->addDefaultsIfNotSet()
									->children()
										->scalarNode('item_class')->defaultValue('Webit\Common\CurrencyBundle\Entity\CurrencyEnabled')->end()
									->end()
								->end()
								->arrayNode('phpcr')->addDefaultsIfNotSet()
									->children()
										->scalarNode('item_class')->defaultValue('Webit\Common\CurrencyBundle\Document\CurrencyEnabled')->end()
										->scalarNode('root')->defaultValue('/currency/enabled')->end()
									->end()
								->end()
							->end()
						->end()
					->end()
				->end();
				
        return $treeBuilder;
    }
}
