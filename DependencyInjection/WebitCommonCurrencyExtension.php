<?php
namespace Webit\Common\CurrencyBundle\DependencyInjection;

use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryConfig;
use Webit\Common\DictionaryBundle\DependencyInjection\Definition\DictionaryDefinitionHelper;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class WebitCommonCurrencyExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );

        $helper = new DictionaryDefinitionHelper($container, $this->getAlias());
        foreach (array('currency', 'currency_enabled') as $name) {
            $dict = $config[$name];
            $dict['dictionary_name'] = $name;
            $dictConfig = $helper->createDictionaryConfig($dict);
            $helper->registerDefinition($dictConfig);
        }

        if ($config['use_extjs']) {
            $loader->load('extjs.yml');
        }
    }
}
