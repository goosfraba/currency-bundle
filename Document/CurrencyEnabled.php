<?php
namespace Webit\Common\CurrencyBundle\Document;

use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemAwareInterface;
use Webit\Common\DictionaryBundle\Annotation as Dict;
use Webit\Common\CurrencyBundle\Model\CurrencyAwareInterface;
use Webit\Common\CurrencyBundle\Model\CurrencyEnabledInterface;
use Webit\Common\CurrencyBundle\Model\CurrencyInterface;
use Webit\Bundle\PHPCRToolsBundle\Document\Generic;

class CurrencyEnabled extends Generic implements CurrencyEnabledInterface,
    CurrencyAwareInterface, DictionaryItemAwareInterface
{
    protected $id;

    /**
     * @Dict\ItemCode(dictionaryName="currency",itemProperty="currency")
     */
    protected $code;

    /**
     * @var bool
     */
    protected $base = false;

    protected $currency;

    public function getCode()
    {
        if ($this->code == null && $this->currency) {
            return $this->currency->getCode();
        }

        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getSymbol()
    {
        return $this->currency ? $this->currency->getSymbol() : null;
    }

    public function setSymbol($symbol)
    {
        return null;
    }

    public function getLabel()
    {
        return $this->currency ? $this->currency->getLabel() : null;
    }

    public function setLabel($label)
    {
        return null;
    }

    public function setCurrency(CurrencyInterface $currency)
    {
        $this->currency = $currency;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getBase()
    {
        return $this->base;
    }

    public function setBase($bool)
    {
        $this->base = $bool;
    }

    public function __sleep()
    {
        return array('id', 'code', 'base', 'currency');
    }

    public function __toString()
    {
        return (string) $this->getCode();
    }
}
