<?php
namespace Webit\Common\CurrencyBundle\Document;

use Webit\Common\CurrencyBundle\Model\CurrencyInterface;
use Webit\Bundle\PHPCRToolsBundle\Document\Generic;

class Currency extends Generic implements CurrencyInterface
{
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $symbol;

    /**
     * @var string
     */
    protected $label;

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function __sleep()
    {
        return array('id', 'code', 'symbol', 'label');
    }

    public function __toString()
    {
        return $this->getCode();
    }
}
