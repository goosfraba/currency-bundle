<?php
namespace Webit\Common\CurrencyBundle\Model;

use Webit\Common\DictionaryBundle\Model\Dictionary\Dictionary;
use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemInterface;

class CurrencyEnabledDictionary extends Dictionary
{
    public function setBaseCurrency(CurrencyEnabledInterface $currencyEnabled)
    {
        $this->getItems();

        foreach ($this->items as $item) {
            $item->setBase(false);
        }
        $currencyEnabled->setBase(true);
    }

    public function updateItem(DictionaryItemInterface $item)
    {
        parent::updateItem($item);
        if ($item->getBase() == true) {
            $this->setBaseCurrency($item);
        }

        return $item;
    }

    public function getBaseCurrency()
    {
        $this->getItems();

        $coll = $this->items->filter(
            function ($currency) {
                return $currency->getBase() == true;
            }
        );

        if ($coll->count() > 0) {
            return $coll->first;
        }

        return null;
    }
}
