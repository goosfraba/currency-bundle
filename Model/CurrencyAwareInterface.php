<?php
namespace Webit\Common\CurrencyBundle\Model;

interface CurrencyAwareInterface
{
    /**
     * @return CurrencyInterface
     */
    public function getCurrency();
}

