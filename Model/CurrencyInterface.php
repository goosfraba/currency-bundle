<?php
namespace Webit\Common\CurrencyBundle\Model;

use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemInterface;

interface CurrencyInterface extends DictionaryItemInterface
{
    public function getCode();

    public function setCode($code);

    public function getSymbol();

    public function setSymbol($symbol);

    public function getLabel();

    public function setLabel($label);
}
