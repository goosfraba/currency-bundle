<?php
namespace Webit\Common\CurrencyBundle\Model;

interface CurrencyEnabledInterface extends CurrencyInterface
{
    public function getBase();

    public function setBase($bool);
}
