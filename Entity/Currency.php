<?php
namespace Webit\Common\CurrencyBundle\Entity;

use Webit\Common\CurrencyBundle\Model\CurrencyInterface;

class Currency implements CurrencyInterface
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $symbol;

    /**
     * @var string
     */
    protected $label;

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function __toString()
    {
        return $this->getCode();
    }
}
