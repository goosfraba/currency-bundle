<?php
namespace Webit\Common\CurrencyBundle\ExtJs;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webit\Bundle\ExtJsBundle\StaticData\StaticDataExposerInterface;

class StaticDataExposer implements StaticDataExposerInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    /**
     * @return array<key, data>
     */
    public function getExposedData()
    {
        $staticData = array();

        $staticData['currency'] = $this->container->get('webit_common_currency.currency_dictionary')->getItems(
        )->getValues();
        $staticData['currency_enabled'] = $this->container->get(
            'webit_common_currency.currency_enabled_dictionary'
        )->getItems()->getValues();

        // FIXME: values as array (not object), remove keys

        return $staticData;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
