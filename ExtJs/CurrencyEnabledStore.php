<?php
namespace Webit\Common\CurrencyBundle\ExtJs;

use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryInterface;
use Webit\Tools\Object\ObjectUpdater;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;
use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;

class CurrencyEnabledStore extends ExtJsStoreAbstract
{
    /**
     * @var DictionaryInterface
     */
    protected $cd;

    protected $options;

    public function __construct(DictionaryInterface $cd, $options = array())
    {
        parent::__construct($options);
        $this->cd = $cd;
    }

    /**
     *
     * @param array $queryParams
     * @param array $filters
     * @param stdClass $sort
     * @param int $page
     * @param int $limit
     * @param int $offset
     * @return ExtJsJson
     */
    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = 1,
        $limit = 25,
        $offset = 0
    ) {
        $collItems = $this->cd->getItems();

        $json = new ExtJsJson();
        $json->setData(array_values($collItems->toArray()));
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    /**
     *
     * @param string $id
     * @return Product
     */
    public function loadModel($id, $queryParams)
    {
        throw new \Exception('Not implemented');
    }

    public function createModels(\Traversable $arModelList)
    {
        $updater = new ObjectUpdater();
        foreach ($arModelList as $key => $item) {
            $arModelList[$key] = $this->cd->updateItem($item);
        }

        $this->cd->commitChanges();

        $json = new ExtJsJson();
        $json->setData($arModelList);
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    public function updateModels(\Traversable $arModelList)
    {
        $updater = new ObjectUpdater();
        foreach ($arModelList as $key => $sItem) {
            $item = $this->cd->getItem($sItem->getCode());
            $updater->fromObject($sItem, $item, array('code'));

            $arModelList[$key] = $this->cd->updateItem($item);
        }

        $this->cd->commitChanges();

        $json = new ExtJsJson();
        $json->setData($arModelList);
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    /**
     *
     * @param string $id
     */
    public function deleteModel($id)
    {
        $response = new ExtJsJson();
        $response->setData(true);
        foreach ($id as $item) {
            $this->cd->removeItem($item);
        }
        $this->cd->commitChanges();

        return $response;
    }

    public function getDataClass()
    {
        return $this->cd->getItemClass();
    }
}
