<?php
namespace Webit\Common\CurrencyBundle\Command;

use Symfony\Component\Yaml\Yaml;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class LoadCurrencyCommand extends ContainerAwareCommand
{
    private $inputFile;

    /**
     *
     * @var UnitManagerInterface
     */
    private $currencyDict;

    private $enabledDict;

    protected function configure()
    {
        parent::configure();
        $this->setName('webit:currency:load')
            ->addArgument('file', InputArgument::OPTIONAL)
            ->setDescription('Load currency list from file');
    }

    /**
     * (non-PHPdoc)
     * @see Symfony\Component\Console\Command.Command::initialize()
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->currencyDict = $this->getContainer()->get('webit_common_currency.currency_dictionary');
        $this->enabledDict = $this->getContainer()->get('webit_common_currency.currency_enabled_dictionary');

        $file = $input->getArgument('file');
        $file = $file ?: $this->getDefaultUnitsFile();

        $this->inputFile = $file;
    }

    private function getDefaultUnitsFile()
    {
        return __DIR__ . '/../Resources/static/currency.yml';
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!is_file($this->inputFile)) {
            $output->writeln('<error>Input file "' . $this->inputFile . '" not found.</error>');

            return false;
        }

        $arYml = Yaml::parse($this->inputFile);
        if (!is_array($arYml)) {
            $output->writeln('<error>Cannot parse input file.</error>');
        }

        $arEnabled = array();

        $arList = isset($arYml['currency']) ? $arYml['currency'] : array();
        foreach ($arList as $code => $arCurrency) {
            $currency = $this->currencyDict->getItem($code) ?: $this->currencyDict->createItem();
            $currency->setCode($code);
            $currency->setSymbol($arCurrency['symbol']);
            $currency->setLabel($arCurrency['label']);

            $this->currencyDict->updateItem($currency);

            if (isset($arCurrency['enabled']) && $arCurrency['enabled'] == true) {
                $arEnabled[$code] = isset($arCurrency['base']) ? $arCurrency['base'] : false;
            }
        }

        $this->currencyDict->commitChanges();

        foreach ($arEnabled as $code => $base) {
            $enabled = $this->enabledDict->getItem($code) ?: $this->enabledDict->createItem();
            $enabled->setCurrency($this->currencyDict->getItem($code));
            $this->enabledDict->updateItem($enabled);
            if ($base) {
                $this->enabledDict->setBaseCurrency($enabled);
            }
        }

        $this->enabledDict->commitChanges();;
    }
}
